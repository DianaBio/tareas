import os

import markdown

from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.conf import settings
from django.http import FileResponse, Http404
from django.core.exceptions import PermissionDenied
from django.contrib import messages


from .models import Estudiante, Tarea, TareaEstudiante, TareaEstudianteEvento

tarea_view_sql = """        
CREATE TEMP VIEW t AS (
WITH tee_latest AS (
SELECT
    tee1.id
FROM tar_tareaestudianteevento tee1
JOIN (
    SELECT tarea_estudiante_id, MAX(tiempo) tiempo
    FROM tar_tareaestudianteevento
    GROUP BY tarea_estudiante_id
) tee2
ON
    tee1.tarea_estudiante_id = tee2.tarea_estudiante_id
    AND tee1.tiempo = tee2.tiempo
)
, entregada_latest AS (
SELECT
    tee1.id
FROM tar_tareaestudianteevento tee1
JOIN (
    SELECT tarea_estudiante_id, MAX(tiempo) tiempo
    FROM tar_tareaestudianteevento
    WHERE tipo_id = 'ENTREGADO'
    GROUP BY tarea_estudiante_id
) tee2
ON
    tee1.tarea_estudiante_id = tee2.tarea_estudiante_id
    AND tee1.tiempo = tee2.tiempo
)
SELECT 
    t.id
    , te.id tarea_estudiante_id
    , t.semana
    , t.nombre
    , entregada.tiempo tiempo_entrega
    , regexp_replace(entregada.adjunto, '^.+/', '') adjunto
    , entregada.adjunto adjunto_path
    , tipo.nombre estado
    , (
        SELECT 1 FROM tar_tareaestudianteevento
        WHERE
            tarea_estudiante_id = te.id
            AND notas IS NOT NULL
            AND notas != ''
        LIMIT 1
    ) tiene_notas
    , u.id user_id
    , u.first_name || ' ' || u.last_name nombre_estudiante
FROM tar_tarea t
JOIN tar_estudiante_cursos ce
ON t.curso_id = ce.curso_id
JOIN tar_estudiante e
ON e.id = ce.estudiante_id
LEFT OUTER JOIN auth_user u
ON u.id = e.user_id
LEFT OUTER JOIN tar_tareaestudiante te
ON
    t.id = te.tarea_id
    AND te.estudiante_id = e.id
LEFT OUTER JOIN tar_tareaestudianteevento tee
ON
    te.id = tee.tarea_estudiante_id
    AND tee.id IN (SELECT id from tee_latest)
LEFT OUTER JOIN tar_tipoevento tipo
ON
    tee.tipo_id = tipo.val
LEFT OUTER JOIN tar_tareaestudianteevento entregada
ON
    te.id = entregada.tarea_estudiante_id
    AND entregada.id IN (SELECT id from entregada_latest)
ORDER BY t.semana DESC
);
"""

def lista_tareas(request):
    ctx = {}
    ctx['first_name'] = request.user.first_name
    ctx['last_name'] = request.user.last_name
    try:
        e = Estudiante.objects.get(user_id=request.user.id)
    except Estudiante.DoesNotExist:
        if request.user.is_staff:
            return redirect('/revisar')
        else:
            raise PermissionDenied()

    # Por el momento, asumir solo un curso activo
    curso = e.cursos.first()
    if curso:
        ctx['curso'] = curso.nombre
    else:
        ctx['curso'] = None

    # Consigue todas las encuestas activas para el estudiante
    encuestainstancias_activas = []
    for c in e.cursos.all():
        if c.encuestainstancia_set.filter(activo=True).exists():
            messages.add_message(request, messages.WARNING,
                '¡Hay nuevas preguntas para responder! Haga clic aquí.',
                extra_tags='link href=/preguntas')

    tareas =  Tarea.objects.raw(tarea_view_sql +
"""
SELECT * FROM t WHERE user_id = %s
"""
        , [request.user.id]
    )

    ctx['tareas'] = tareas
    return render(request, 'tarea_list.html', context=ctx)


class RevisarTareasListView(ListView):

    template_name = 'revisar_tareas.html'

    def get_context_data(self, **kwargs):
        context = super(RevisarTareasListView, self).get_context_data(**kwargs)
        return context

    def get_queryset(self):
        queryset =  Tarea.objects.raw(tarea_view_sql +
"""
SELECT * FROM t
WHERE tarea_estudiante_id IS NOT NULL
ORDER BY
    estado = 'Aceptada'
    , tiempo_entrega DESC
"""
        )
        return queryset

    def get(self, request):
        if request.user.is_staff:
            return super().get(request)
        raise PermissionDenied()


def attachment(request):
    if request.method == 'POST':
        if 'adjunto' in  request.FILES:
            t = Tarea.objects.get(id=request.POST['tarea_id'])
            e = Estudiante.objects.get(user_id=request.user.id)

            # Comprobar si ya existe TareaEstudiante
            te_id = request.POST['tarea_estudiante_id']
            if te_id:
                te = TareaEstudiante.objects.get(id=te_id)
            else:
                # Si es un nuevo adjunto, crear nuevo TareaEstudiante
                te = TareaEstudiante(tarea=t, estudiante=e)
                te.save()

            tee = TareaEstudianteEvento(
                tarea_estudiante=te,
                tipo_id='ENTREGADO',
                user=request.user,
                adjunto=request.FILES['adjunto']
            )
            tee.save()

            return redirect('/')
        return redirect('/')

def descargar_tarea(request):
    tee = TareaEstudianteEvento.objects.get(adjunto=request.path[1:])
    # Permitir usuario si crearon el archivo o son administradores
    if tee.user == request.user or request.user.is_staff:
        filename = os.path.join('media', tee.adjunto.path)
        response = FileResponse(open(filename, 'rb'))
        return response
    raise PermissionDenied()

def aprobar_rechazar(request):
    if request.method == 'POST':
        t = Tarea.objects.get(id=request.POST['tarea_id'])

        te_id = request.POST['tarea_estudiante_id']
        te = TareaEstudiante.objects.get(id=te_id)

        if 'aceptar' in request.POST:
            estado = 'ACEPTADO'
        elif 'rechazar' in request.POST:
            estado = 'RECHAZADO'
        else:
            raise ValueError('petición malformada')

        ctx = {
            'estudiante': te.estudiante.user.first_name + ' ' + te.estudiante.user.last_name,
            'semana_tarea': t.semana,
            'nombre_tarea': t.nombre,
            'estado': estado,
            'tarea_estudiante_id': te_id,
        }

        return render(request, 'agregar_nota.html', context=ctx)

def guardar_nota(request):
    if request.method == 'POST':
        te_id = request.POST['tarea_estudiante_id']
        te = TareaEstudiante.objects.get(id=te_id)

        tee = TareaEstudianteEvento(
            tarea_estudiante=te,
            tipo_id=request.POST['estado'],
            user=request.user,
            notas=request.POST['nota'] or None,
        )
        tee.save()

        return redirect('/revisar')

def detalles_tarea_entregada(request, tarea_estudiante_id):

    try:
        te = TareaEstudiante.objects.get(id=tarea_estudiante_id)
    except:
        raise PermissionDenied()

    eventos = list(te.tareaestudianteevento_set.order_by('-tiempo').all())

    if te.estudiante.user == request.user or request.user.is_staff:
        ctx = {
            'eventos': eventos,
            'semana_tarea': te.tarea.semana,
            'nombre_tarea': te.tarea.nombre,
            'estado': eventos[0].tipo,
        }

        return render(request, 'detalles_tarea_entregada.html', context=ctx)
    raise PermissionDenied()


def detalles_tarea(request, tarea_id):

    try:
        t = Tarea.objects.get(id=tarea_id)
    except:
        raise Http404()

    descripcion_html = markdown.markdown(t.descripcion, extensions=['sane_lists'])

    ctx = {
        'nombre_tarea': t.nombre,
        'semana_tarea': t.semana,
        'descripcion_tarea': descripcion_html,
        'imagen_tarea': t.imagen,
    }

    return render(request, 'detalles_tarea.html', context=ctx)

def descargar_imagen(request):
    ruta_imagen=request.path[1:]
    filename = os.path.join('media', ruta_imagen)
    return FileResponse(open(filename, 'rb'))

def lista_estudiantes(request):
    if request.user.is_staff:
        ctx = {}
        ctx['estudiantes'] = Estudiante.objects.all()
        return render(request, 'estudiantes_list.html', context=ctx)
    raise PermissionDenied()


def estudiante_tareas(request, estudiante_id):
    if request.user.is_staff:
        e = Estudiante.objects.get(id=estudiante_id)
        ctx = {}
        ctx['object_list'] =  list(Tarea.objects.raw(tarea_view_sql +
"""
SELECT * FROM t
WHERE user_id = %s
"""
        , [e.user_id]))
        return render(request, 'revisar_tareas.html', context=ctx)
    raise PermissionDenied()

'''
'''

