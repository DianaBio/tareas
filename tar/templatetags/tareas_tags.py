from django import template

register = template.Library()

@register.filter(name='startswith')
def startswith(list_, s):
    return [e for e in list_.split(' ') if e.startswith(s)][0]
