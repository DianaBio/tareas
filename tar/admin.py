from django.contrib import admin

from django.contrib.auth.models import User
from .models import (
    Curso,
    Estudiante,
    Tarea,
    TareaEstudiante,
    TareaEstudianteEvento,
    TipoEvento,
)

class TareasAdminSite(admin.AdminSite):
    site_header = 'Tareas'

admin_site = TareasAdminSite(name='tareaadmin')
admin_site.register(User)
admin_site.register(Curso)
admin_site.register(Estudiante)
admin_site.register(Tarea)
admin_site.register(TareaEstudiante)
admin_site.register(TareaEstudianteEvento)
admin_site.register(TipoEvento)

