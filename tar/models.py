import locale

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from django.conf import settings
from django.utils import timezone

class Curso(models.Model):

    id = models.BigAutoField(primary_key=True)

    nombre = models.CharField(
        _('Nombre'),
        max_length=255,
    )

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Cursos'


class Estudiante(models.Model):

    id = models.BigAutoField(primary_key=True)


    user = models.ForeignKey(
        User,
        models.DO_NOTHING,
        verbose_name=_('usuario'),
    )

    cursos = models.ManyToManyField(
        Curso,
        verbose_name=_('cursos'),
        blank=True,
    )

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name_plural = 'Estudiantes'


class Tarea(models.Model):

    id = models.BigAutoField(primary_key=True)

    nombre = models.CharField(
        _('Nombre'),
        max_length=255,
    )

    semana = models.IntegerField(
        _('Semana'),
    )

    curso = models.ForeignKey(
        Curso,
        models.DO_NOTHING,
        verbose_name=_('cursos'),
    )

    descripcion = models.TextField(
        _('Descripción'),
        max_length=16384,
    )

    imagen = models.FileField(
        upload_to='imagen',
        null=True,
        blank=True,
    )

    def __str__(self):
        return "Semana {}: {}".format(self.semana, self.nombre)

    class Meta:
        verbose_name_plural = 'Tarea asignada'


class TareaEstudiante(models.Model):

    id = models.BigAutoField(primary_key=True)

    tarea = models.ForeignKey(
        Tarea,
        models.DO_NOTHING,
        verbose_name=_('Tarea'),
    )

    estudiante = models.ForeignKey(
        Estudiante,
        models.DO_NOTHING,
        verbose_name=_('Estudiante'),
    )

    tiempo_entrega = models.DateTimeField(
        _('Tiempo entrega'),
        default=timezone.now
    )

    def __str__(self):
        return "Tarea {} entregada por {}".format(self.tarea, self.estudiante)

    class Meta:
        verbose_name_plural = 'Tarea entregada'


class TipoEvento(models.Model):

    val = models.CharField(
        _('Val'),
        max_length=255,
        primary_key=True,
    )

    nombre = models.CharField(
        _('Nombre'),
        max_length=255,
    )

    verbo = models.CharField(
        _('Verbo (pasado)'),
        max_length=255,
    )

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'TipoEventos'


class TareaEstudianteEvento(models.Model):
    
    id = models.BigAutoField(primary_key=True)

    tipo = models.ForeignKey(
        TipoEvento,
        models.DO_NOTHING,
        verbose_name=_('Tipo evento'),
    )

    notas = models.TextField(
        _('Notas'),
        blank=True,
        null=True,
    )

    tarea_estudiante = models.ForeignKey(
        TareaEstudiante,
        models.DO_NOTHING,
        verbose_name=_('Tarea entregada'),
    )

    user = models.ForeignKey(
        User,
        models.DO_NOTHING,
        verbose_name=_('usuario'),
    )

    tiempo = models.DateTimeField(
        _('Tiempo'),
        default=timezone.now
    )

    @property
    def verbo(self):
        return self.tipo.verbo

    def tarea_path(instance, filename):
        # se guarda el archive en MEDIA_ROOT/adjuntos/<user>/curso_<id>/tarea_<id>/<filename>
        num_adjuntos = TareaEstudianteEvento.objects.filter(
            tarea_estudiante=instance.tarea_estudiante,
            adjunto__isnull=False
        ).count()

        return 'adjuntos/{0}/curso_{1}/tarea_{2}/v{3}/{4}'.format(
            instance.user.username,
            instance.tarea_estudiante.tarea.curso_id,
            instance.tarea_estudiante.tarea_id,
            num_adjuntos,
            filename)

    adjunto = models.FileField(
        upload_to=tarea_path,
        null=True,
        blank=True,
    )

    def __str__(self):
        return "{}: {}".format(self.tipo, str(self.tarea_estudiante))

    class Meta:
        verbose_name_plural = 'Eventos tareas'


