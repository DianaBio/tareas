# Aplicación web de tareas y preguntas para cursos educativos

[Click here for English](#web-application-for-educational-courses-to-manage-homework-and-create-quizzes)

![](https://gitlab.com/DianaBio/tareas/-/raw/master/static/tareas.gif)

Una aplicación web usando Python, Django y PostgreSQL que ofrece varias
capacidades para administrar cursos educativos:

- Inscribir estudiantes en uno o más cursos
- Administrar tareas
  - Usar Markdown e incluir imagenes para describir cada tarea
  - Asignar tareas a estudiantes según el curso
  - Recibir tareas a través de un formulario para subir archivos
  - Revisar tareas y aceptar o devolver para correcciones
  - Interfaz eficiente para revisar gran cantidad de tareas
- Crear cuestionarios interactivos
  - Crear preguntas de selección múltiple (con única respuesta o con respuesta múltiple)
  - Definir cualquier número de preguntas y sus opciones en una sola acción con
    un formato de texto simple
  - Controlar la presentación y el avance de las preguntas, para que todos los
    estudiantes en una clase presencial o virtual ven y responden al mismo
    tiempo
  - Mostrar respuestas en un gráfico de barras que se actualiza automáticamente
    cuando los estudiantes guardan sus respuestas

# Web application for educational courses to manage homework and create quizzes

A web application using Python, Django, and PostgreSQL that offers various
features to administer educational courses:

- Enroll students in one or more courses
- Administer homework
  - Use Markdown and include images to describe homework
  - Assign homework to students based on the course
  - Receive homework through an upload form
  - Review homework and accept or send it back for corrections
  - Efficient interface to review a large quantity of homeworks
- Create interactive quizzes
  - Create multiple choice questions (single or multiple answer)
  - Define any number of questions and their options in a single action with a
    simple text format
  - Control the presentaction and progression of the questions, so that all
    students in an in-person or virtual class see the same question and respond
    at the same time
  - Show the responses in a bar graph that updates automatically when the
    students submit their responses
