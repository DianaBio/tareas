import yaml

from django.db import connection
from django.shortcuts import render, redirect
from tar.models import Estudiante, Curso
from django.http import HttpResponse
from .models import Respuesta, PreguntaOpcion, EncuestaInstancia, PreguntaInstancia, Pregunta
from django.core.exceptions import PermissionDenied
from django.utils import timezone
from django.db import transaction

import logging
logger = logging.getLogger(__name__)


pregunta_sql = '''
SELECT o.orden, o.id, o.texto, count(pi.*) conteo
FROM enc_preguntainstancia pi
JOIN enc_pregunta p
    ON p.id = pi.pregunta_id
JOIN enc_preguntaopcion o
    ON o.pregunta_id = p.id
LEFT OUTER JOIN enc_respuesta r
    ON
        r.pregunta_opcion_id = o.id
        AND r.pregunta_instancia_id = pi.id
WHERE pi.id = %s
GROUP BY o.orden, o.id, o.texto
ORDER BY o.orden
;
'''

encuesta_instancia_ultimo_cambio_sql = '''
select max(tiempo) from (
    select tiempo
    from enc_encuestainstancia
    where id = %s
    UNION
    select tiempo
    from enc_preguntainstancia
    where encuesta_instancia_id = %s
    UNION
    select r.tiempo
    from enc_preguntainstancia pi
    join enc_respuesta r
    on r.pregunta_instancia_id = pi.id
    where pi.encuesta_instancia_id = %s
) t;
'''


def encuesta(request):
    user_id = request.user.id

    if request.user.is_staff and request.method == 'POST':
        return redirect('/preguntas')

    if not request.user.is_staff:
        e = Estudiante.objects.get(user_id=user_id)

        if request.method == 'POST':
            pregunta_opcion_ids = request.POST.getlist('opcion_id')
            pregunta_instancia_id = request.POST['pregunta_instancia_id']
            respuestas_existentes = Respuesta.objects.filter(
                pregunta_instancia_id=pregunta_instancia_id,
                estudiante_id=e.id,
            )
            if not respuestas_existentes.exists():
                for po_id in pregunta_opcion_ids:
                    respuesta = Respuesta(
                        pregunta_opcion_id=po_id,
                        pregunta_instancia_id=pregunta_instancia_id,
                        estudiante_id=e.id,
                    )
                    respuesta.save()
            return redirect('/preguntas')

        cursos = e.cursos.all()
    else:
        cursos = Curso.objects.all()

    # Consigue todas las encuestas activas para el estudiante
    encuestainstancias_activas = []
    for c in cursos:
        encuestainstancias_activas.extend(
            list(c.encuestainstancia_set.filter(activo=True)))

    ctx = {}
    if not request.user.is_staff:
        ctx['first_name'] = request.user.first_name
        ctx['last_name'] = request.user.last_name

    # Solo debe ser una encuesta activa
    if encuestainstancias_activas:
        encuestainstancia = encuestainstancias_activas[0]

        tiempo_ultimo_cambio = 0;
        with connection.cursor() as cursor:
            cursor.execute(
                encuesta_instancia_ultimo_cambio_sql,
                [encuestainstancia.id, encuestainstancia.id, encuestainstancia.id])
            res = cursor.fetchone()
            tiempo_ultimo_cambio = int(res[0].timestamp())

        # Consigue primer pregunta activa
        pregunta_instancia = (
            encuestainstancia.preguntainstancia_set
            .filter(activo=True)
            .order_by('-pregunta__orden')
            .first()
        )
        if pregunta_instancia is not None:
            pregunta = pregunta_instancia.pregunta
            respuestas_qs = Respuesta.objects.filter(
                pregunta_instancia_id=pregunta_instancia.id,
            )
            if not request.user.is_staff:
                respuestas_qs = respuestas_qs.filter(estudiante_id=e.id)
            respuestas = list(respuestas_qs)

            ctx['tiempo_ultimo_cambio'] = tiempo_ultimo_cambio
            ctx['pregunta'] = pregunta
            ctx['pregunta_instancia'] = pregunta_instancia
            ctx['encuesta_instancia_id'] = pregunta_instancia.encuesta_instancia.id
            ctx['opciones'] = pregunta.preguntaopcion_set.order_by('orden').all()
            if respuestas:
                ctx['respuestas'] = respuestas
                resultados_ =  list(PreguntaOpcion.objects.raw(
                    pregunta_sql,
                    [pregunta_instancia.id]))
                conteo_max = 0
                resultados = []
                for r in resultados_:
                    r = r.__dict__
                    conteo_max = max(r['conteo'], conteo_max)
                    resultados.append(r)
                max_y = max(conteo_max, 8)

                for r in resultados:
                    r['bar_value'] = int(100*float(r['conteo'])/max_y)
                ctx['resultados'] = resultados

                if request.user.is_staff:
                    if (encuestainstancia.preguntainstancia_set
                        .filter(activo=False)
                        .exists()
                    ):
                        ctx['texto_boton_admin'] = 'Siguiente pregunta'
                    else:
                        ctx['texto_boton_admin'] = 'Cerrar preguntas'
                # Lógico para mostrar o no los resultados
                ctx['mostrar_resultados'] = (
                    (
                        not request.user.is_staff
                        and len(respuestas) > 0
                    ) or (
                        request.user.is_staff
                        and pregunta_instancia.mostrar_resultados
                ))

    return render(request, 'encuesta.html', context=ctx)

def encuesta_instancia_ultimo_cambio(request, encuesta_instancia_id):
    with connection.cursor() as cursor:
        cursor.execute(
            encuesta_instancia_ultimo_cambio_sql,
            [encuesta_instancia_id, encuesta_instancia_id, encuesta_instancia_id])
        res = cursor.fetchone()
        tiempo_ultimo_cambio = int(res[0].timestamp())
        return HttpResponse(tiempo_ultimo_cambio)

def encuesta_instancia_siguiente_pregunta(request, encuesta_instancia_id):
    if request.user.is_staff:
        siguiente_pregunta = (PreguntaInstancia.objects
            .filter(
                encuesta_instancia_id=encuesta_instancia_id,
                activo=False
            )
            .order_by('pregunta__orden')
            .first()
        )
        if siguiente_pregunta:
            siguiente_pregunta.activo = True
            siguiente_pregunta.tiempo = timezone.now()
            siguiente_pregunta.save()
        else:
            ei = EncuestaInstancia.objects.get(id=encuesta_instancia_id)
            ei.activo = False
            ei.tiempo = timezone.now()
            ei.save()
    else:
        raise PermissionDenied()

    return redirect('/preguntas')

def mostrar_resultados(request):
    if request.user.is_staff:
        pregunta_instancia_id = request.POST['pregunta_instancia_id']
        p = PreguntaInstancia.objects.get(id=pregunta_instancia_id)
        p.mostrar_resultados = True
        p.save()
    else:
        raise PermissionDenied()
    return redirect('/preguntas')

def procesar_preguntas_yaml(preguntas_yaml):
    en_block = 0
    en_opciones = 0
    salida = ''
    for line in preguntas_yaml.splitlines():
        if not en_block and line.strip() != '':
            en_block = 1
            salida += '---\n' + line + '\n'
        elif line.strip() == '':
            en_block = 0
            en_opciones = 0
            salida += '\n'
        elif not en_opciones and line.startswith('-'):
            salida += 'opciones:\n' + line + '\n'
            en_opciones = 1
        else:
            salida += line + '\n'
    return salida


def configurar_preguntas(request, encuesta_plantilla_id):
    ctx = {}
    ctx['encuesta_plantilla_id'] = encuesta_plantilla_id
    if not request.user.is_staff:
        raise PermissionDenied()
    if request.method == 'POST':
        preguntas_yaml = request.POST['preguntas_yaml']
        preguntas_yaml = procesar_preguntas_yaml(preguntas_yaml)
        #try:
        # Iniciar una transacción en la BBDD
        with transaction.atomic():
            pregunta_ids = []
            for i, p in enumerate(yaml.safe_load_all(preguntas_yaml)):
                if len(p) != 2:
                    raise ValueError('pregunta malformada')
                opciones = p['opciones']
                for k in p.keys():
                    if k != 'opciones':
                        id_editor = k
                        texto = p[k]

                respuesta_multiple = False
                if texto[-1] == '*':
                    respuesta_multiple = True
                    texto = texto[:-1]
                try:
                    p = Pregunta.objects.get(
                        encuesta_plantilla_id=encuesta_plantilla_id,
                        id_editor=id_editor)
                    p.texto = texto
                    p.respuesta_multiple = respuesta_multiple
                    p.orden = i+1
                except Pregunta.DoesNotExist:
                    p = Pregunta(
                        encuesta_plantilla_id=encuesta_plantilla_id,
                        orden=i+1,
                        texto=texto,
                        id_editor=id_editor,
                        respuesta_multiple=respuesta_multiple,
                    )
                p.save()
                pregunta_ids.append(p.id)
                opcion_ids = []
                for i, texto in enumerate(opciones):
                    try:
                        o = PreguntaOpcion.objects.get(pregunta=p, orden=i+1)
                        o.texto = texto
                    except PreguntaOpcion.DoesNotExist:
                        o = PreguntaOpcion(pregunta=p, orden=i+1, texto=texto)
                    o.save()
                    opcion_ids.append(o.id)
                PreguntaOpcion.objects.filter(pregunta=p).exclude(id__in=opcion_ids).delete()
            Pregunta.objects.filter(
                encuesta_plantilla_id=encuesta_plantilla_id
            ).exclude(id__in=pregunta_ids).delete()
        #except yaml.YAMLError as exc:
        #    print(exc)
        return redirect('/preguntas/{}/configurar'.format(encuesta_plantilla_id))
    elif request.method == 'GET':
        preguntas = (Pregunta.objects
            .filter(encuesta_plantilla_id=encuesta_plantilla_id)
            .order_by('orden')
        )
        ctx['preguntas'] = preguntas
        ctx['cursos'] = Curso.objects.all()
    return render(request, 'configurar_preguntas.html', context=ctx)

def crear_encuesta_desde_plantilla(request):
    if request.user.is_staff and request.method == 'POST':
        encuesta_plantilla_id = request.POST['encuesta_plantilla_id']
        curso_id = request.POST['curso']
        # Iniciar una transacción en la BBDD
        with transaction.atomic():
            ei = EncuestaInstancia(
                encuesta_plantilla_id=encuesta_plantilla_id,
                curso_id=curso_id,
            )
            ei.save()
            preguntas = Pregunta.objects.filter(
                encuesta_plantilla_id=encuesta_plantilla_id,
            ).order_by('orden')
            for p in preguntas:
                pi = PreguntaInstancia(
                    pregunta=p,
                    encuesta_instancia=ei,
                )
                pi.save()
        return redirect('/encuesta/{}/configurar'.format(ei.id))
    else:
        raise PermissionDenied()
