# Generated by Django 3.0.10 on 2020-12-06 19:39

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('enc', '0003_preguntainstancia_pregunta'),
    ]

    operations = [
        migrations.AddField(
            model_name='preguntainstancia',
            name='tiempo',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Tiempo'),
        ),
        migrations.AddField(
            model_name='preguntaopcioneleccion',
            name='tiempo',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Tiempo'),
        ),
        migrations.AlterField(
            model_name='preguntainstancia',
            name='activo',
            field=models.BooleanField(default=False, verbose_name='Activo'),
        ),
    ]
