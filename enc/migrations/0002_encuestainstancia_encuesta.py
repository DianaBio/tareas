# Generated by Django 3.0.10 on 2020-12-03 17:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('enc', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='encuestainstancia',
            name='encuesta',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='enc.Encuesta', verbose_name='encuesta'),
            preserve_default=False,
        ),
    ]
