from django.utils import timezone
from django.db import models
from django.utils.translation import gettext_lazy as _

from tar.models import Estudiante, Curso

class EncuestaPlantilla(models.Model):
    """EncuestaPlantilla es una colleción ordenada de Preguntas que sirven para
    crear una EncuestaInstancia. Su función principal es permitir el reuso
    fácil de una colleción de Preguntas en más que una EncuestaInstancia.
    """

    id = models.BigAutoField(primary_key=True)

    nombre = models.CharField(
        _('nombre'),
        max_length=255,
    )

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'EncuestaPlantilla'


class Pregunta(models.Model):
    """Pregunta representa una pregunta de selección múltiple. Cada Pregunta
    pertenece a una EncuestaPlantilla y tiene un orden respeto a las otras
    Preguntas que hacen parte de la EncuestaPlantilla.

    Una Pregunta tiene al menos una PreguntaOpción asociada, que representa una
    opción posible para una respuesta.

    Una Pregunta puede permitir una sola respuesta (por defecto) o varias
    (respuesta_multiple=True).
    """

    id = models.BigAutoField(primary_key=True)

    encuesta_plantilla = models.ForeignKey(
        EncuestaPlantilla,
        models.CASCADE,
        verbose_name=_('encuesta plantilla'),
    )

    orden = models.IntegerField(
        _('Orden'),
    )

    texto = models.CharField(
        _('Pregunta'),
        max_length=255,
    )
    
    respuesta_multiple = models.BooleanField(
        default=False,
        verbose_name=_('Respuesta múltiple'),
    )

    id_editor = models.IntegerField(
        _('ID usado en el editor'),
    )

    def __str__(self):
        return "{}: {}".format(self.encuesta_plantilla, self.texto)

    class Meta:
        verbose_name_plural = 'Preguntas'

class PreguntaOpcion(models.Model):
    """PreguntaOpcion representa una respuesta posible a una Pregunta.

    Cada PreguntaOpcion tiene un orden relativo a las otras PreguntaOpciones
    asociadas con la Pregunta.
    """

    id = models.BigAutoField(primary_key=True)

    pregunta = models.ForeignKey(
        Pregunta,
        models.CASCADE,
        verbose_name=_('pregunta'),
    )

    orden = models.IntegerField(
        _('Orden'),
    )

    texto = models.CharField(
        _('Texto'),
        max_length=255,
    )
    def __str__(self):
        return self.texto

    class Meta:
        verbose_name_plural = 'Opciones para Pregunta'


class EncuestaInstancia(models.Model):
    """EncuestaInstancia representa una la realización de una encuesta con los
    Estudiantes de un Curso dado.

    Una EncuestaInstancia se genera desde una EncuestaPlantilla.

    Se muestra a los estudiantes la EncuestaInstancia activa. Solo debe ser una
    EncuestaInstancia activa en un momento dado.
    """

    id = models.BigAutoField(primary_key=True)

    curso = models.ForeignKey(
        Curso,
        models.CASCADE,
        verbose_name=_('cursos'),
    )

    encuesta_plantilla = models.ForeignKey(
        EncuestaPlantilla,
        models.CASCADE,
        verbose_name=_('encuesta plantilla'),
    )

    activo = models.BooleanField(
        default=True,
        verbose_name=_('Activo'),
    )

    tiempo = models.DateTimeField(
        _('Tiempo'),
        default=timezone.now
    )

    def __str__(self):
        return "{} - {}".format(self.curso, self.encuesta_plantilla)


class PreguntaInstancia(models.Model):
    """PreguntaInstancia mantiene el estado mutable de una Pregunta asociado con una
    EncuestaInstancia. Este estado mutable consiste de que si la
    PreguntaInstancia es la instancia activa, si se deben mostrar los
    resultados y el orden.

    El orden de una PreguntaInstancia hereda su orden desde la Pregunta
    asociada, pero este se puede cambiar cuando se agrega nuevas preguntas a
    una EncuestaInstancia.
    """

    id = models.BigAutoField(primary_key=True)

    encuesta_instancia = models.ForeignKey(
        EncuestaInstancia,
        models.CASCADE,
        verbose_name=_('encuesta instancia'),
    )

    pregunta = models.ForeignKey(
        Pregunta,
        models.CASCADE,
        verbose_name=_('Pregunta'),
    )


    activo = models.BooleanField(
        default=False,
        verbose_name=_('Activo'),
    )

    tiempo = models.DateTimeField(
        _('Tiempo'),
        default=timezone.now
    )

    mostrar_resultados = models.BooleanField(
        default=False,
        verbose_name=_('Mostrar resultados'),
    )

    orden = models.IntegerField(
        _('Orden'),
        blank=True,
    )

    def save(self, *args, **kwargs):
        # Si estamos guardando una nueva PreguntaInstancia,
        # fijar el orden al orden de la Pregunta asociada.
        if self._state.adding:
            self.orden = self.pregunta.orden
        super().save(*args, **kwargs)

    def __str__(self):
        return "{}: {}".format(self.encuesta_instancia, self.pregunta)


class Respuesta(models.Model):
    """Respuesta representa una elección de una PreguntaOpción por parte de un
    Estudiante.

    Una PreguntaInstancia puede tener varias Respuestas de un Estudiante dado
    si corresponde a una Pregunta de respuesta múltiple.
    """

    pregunta_opcion = models.ForeignKey(
        PreguntaOpcion,
        models.CASCADE,
        verbose_name=_('Opción para Pregunta'),
    )

    pregunta_instancia = models.ForeignKey(
        PreguntaInstancia,
        models.CASCADE,
        verbose_name=_('Pregunta instancia'),
    )

    estudiante = models.ForeignKey(
        Estudiante,
        models.CASCADE,
        verbose_name=_('Estudiante'),
    )

    tiempo = models.DateTimeField(
        _('Tiempo'),
        default=timezone.now
    )

    def __str__(self):
        return "Pregunta {}: {} eligió {}".format(self.pregunta_opcion.pregunta.id, self.estudiante, self.pregunta_opcion.id)

    class Meta:
        verbose_name_plural = 'Respuesta para Pregunta'

