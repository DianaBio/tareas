from django.contrib import admin
from tar.admin import admin_site
from enc.models import (
    EncuestaPlantilla,
    EncuestaInstancia,
    Pregunta,
    PreguntaInstancia,
    PreguntaOpcion,
    Respuesta,
)

class PreguntaOpcionInline(admin.TabularInline):
    model = PreguntaOpcion

class PreguntaAdmin(admin.ModelAdmin):
    inlines = [
        PreguntaOpcionInline,
    ]

class PreguntaInstanciaInline(admin.TabularInline):
    model = PreguntaInstancia

class EncuestaInstanciaAdmin(admin.ModelAdmin):
    inlines = [
        PreguntaInstanciaInline,
    ]


admin_site.register(EncuestaPlantilla)
admin_site.register(EncuestaInstancia, EncuestaInstanciaAdmin)
admin_site.register(Pregunta, PreguntaAdmin)
admin_site.register(PreguntaInstancia)
admin_site.register(PreguntaOpcion)
admin_site.register(Respuesta)
