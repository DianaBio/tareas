from django.conf import settings
from django.contrib.auth.middleware import RemoteUserMiddleware

class HttpHeaderAuthMiddleware(RemoteUserMiddleware):
    header = 'HTTP_' + settings.HTTP_AUTH_HEADER.upper().replace('-', '_')
