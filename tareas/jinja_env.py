from jinja2.environment import Environment
from tareas.template_filters import orden_como_letra

class JinjaEnvironment(Environment):

    def __init__(self,**kwargs):
        super(JinjaEnvironment, self).__init__(**kwargs)
        self.filters['orden_como_letra'] = orden_como_letra
