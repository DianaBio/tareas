"""tareas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from tar.admin import admin_site
from django.urls import path, re_path
from tar.views import (
    lista_tareas,
    RevisarTareasListView,
    attachment,
    descargar_tarea,
    descargar_imagen,
    aprobar_rechazar,
    guardar_nota,
    detalles_tarea,
    detalles_tarea_entregada,
    lista_estudiantes,
    estudiante_tareas,
)
from enc.views import (
    encuesta,
    encuesta_instancia_ultimo_cambio,
    encuesta_instancia_siguiente_pregunta,
    configurar_preguntas,
    mostrar_resultados,
    crear_encuesta_desde_plantilla,
)
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin_site.urls),
    path('', lista_tareas, name='lista-tareas'),
    path('revisar', RevisarTareasListView.as_view(), name='revisar-tareas'),
    path('revisar/aprobar_rechazar', aprobar_rechazar, name='revisar-tareas'),
    path('revisar/guardar_nota', guardar_nota, name='guardar-nota'),
    path('tarea/<tarea_id>', detalles_tarea, name='detalles-tarea'),
    path('tarea/entregada/<tarea_estudiante_id>', detalles_tarea_entregada, name='detalles-tarea-entregada'),
    path('adjuntar', attachment, name='adjuntar-tarea'),
    re_path('^adjuntos/', descargar_tarea, name='descargar-tarea'),
    re_path('^imagen/', descargar_imagen, name='descargar-imagen'),
    path('estudiantes', lista_estudiantes, name='lista-estudiantes'),
    path('estudiante/<estudiante_id>', estudiante_tareas, name='estudiante-tareas'),
    path('preguntas', encuesta, name='encuesta'),
    path('api/encuesta_instancia/<encuesta_instancia_id>/ultimo_cambio',
        encuesta_instancia_ultimo_cambio, name='encuesta-ultimo-cambio'),
    path('encuesta_instancia/<encuesta_instancia_id>/siguiente_pregunta',
        encuesta_instancia_siguiente_pregunta, name='encuesta-siguiente-pregunta'),
    path('preguntas/<encuesta_plantilla_id>/configurar', configurar_preguntas, name='configurar-preguntas'),
    path('preguntas/mostrar_resultados', mostrar_resultados, name='mostrar-resultados'),
    path('encuesta_instancia/nueva', crear_encuesta_desde_plantilla, name='crear-encuesta-desde-plantilla'),
]
